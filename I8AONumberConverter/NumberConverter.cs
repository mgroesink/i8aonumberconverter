﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace I8AONumberConverter
{
    public static class NumberConverter
    {
        #region Methods
        /// <summary>
        /// Converts a binary value to decimal.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">This binary value is not allowed</exception>
        public static long BinToDec(string input)
        {
            input = input.Replace(" ", ""); // Remove all spaces from input

            // Check if all characters are 0 or 1
            foreach (var c in input)
            {
                if (c != '0' && c != '1')
                {
                    throw new ArgumentException("Not a valid binary value");
                }
            }

            input = ReverseString(input); // Reverse the input string

            long result = 0;

            // Do the actual conversion
            // For explanation of binary values check
            // https://geocachen.nl/geocaching/geocache-puzzels-oplossen/binaire-code/
            // or check this video on Youtube: https://youtu.be/VLflTjd3lWA
            for (int i = 0; i < input.Length; i++)
            {
                result += (int)Math.Pow(2, i) * int.Parse(input[i].ToString());
            }
            return result;
        }

        /// <summary>
        /// Converts a hexadecimal value to decimal.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">
        /// Hex values have to start with 0X
        /// or
        /// Not a valid hexadecimal value
        /// </exception>
        public static long HexToDec(string input)
        {
            if (!input.ToLower().StartsWith("0x"))
            {
                throw new ArgumentException("Hex values have to start with 0x");
            }
            else
            {
                input = input.Substring(2); //Strip the first two characters (0x)
            }

            input = input.Replace(" ", ""); // Remove all spaces from input
            input = input.ToUpper(); // Change all characters to uppercase

            long result = 0;

            //TODO: add your code to validate the user input
            string hexchars = "0123456789ABCDEF";
            foreach (var c in input)
            {
                if (!hexchars.Contains(c.ToString()))
                {
                    throw new ArgumentException("Not a valid hexadecimal value");
                }
            }
            //TODO: add your code to convert from hexadecimal to decimal


            input = ReverseString(input); // Reverse the input string

            // Do the actual conversion
            // For explanation of hexadecimal values check
            // https://www.beterrekenen.nl/website/index.php?pag=260
            // or check this video on Youtube: https://youtu.be/_TcqGAF0Lu8
            for (int i = 0; i < input.Length; i++)
            {
                int val = 0;
                switch (input[i])
                {
                    case 'A':
                        val = 10;
                        break;
                    case 'B':
                        val = 11;
                        break;
                    case 'C':
                        val = 12;
                        break;
                    case 'D':
                        val = 13;
                        break;
                    case 'E':
                        val = 14;
                        break;
                    case 'F':
                        val = 15;
                        break;
                    default:
                        val = int.Parse(input[i].ToString());
                        break;
                }
                result += (int)Math.Pow(16, i) * val;
            }
            return result;
        }

        /// <summary>
        /// Reverses a string.
        /// </summary>
        /// <param name="text">The string to reverse.</param>
        /// <returns></returns>
        public static string ReverseString(string text)
        {
            string reversed = "";
            for (int i = 0; i < text.Length; i++)
            {
                reversed = text[i] + reversed;
            }
            return reversed;
        }

        #endregion
    }
}