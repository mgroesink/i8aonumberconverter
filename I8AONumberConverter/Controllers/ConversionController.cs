﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I8AONumberConverter.Controllers
{
    public class ConversionController : Controller
    {
        // GET: Conversion
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string input)
        {
            if(Request.HttpMethod.ToLower() != "get")
            {
                string s = Request.HttpMethod;
            }
            // Check if a hexadecimal value is given (starts with 0x)
            if(input.ToLower().StartsWith("0x"))
            {
                try
                {
                    long dec = NumberConverter.HexToDec(input);
                    ViewBag.Decimal = dec;
                }

                catch (ArgumentException ex)
                {
                    ViewBag.Error = ex.Message;
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.Message;
                }
                finally
                {
                    ViewBag.Input = input;
                }
            }
            else
            {
                try
                {
                    long dec = NumberConverter.BinToDec(input);
                    ViewBag.Decimal = dec;

                }

                catch (ArgumentException ex)
                {
                    ViewBag.Error = ex.Message;
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.Message;
                }
                finally
                {
                    ViewBag.Input = input;
                }
            }

            return View();
        }
    }
}