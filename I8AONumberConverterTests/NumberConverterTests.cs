﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using I8AONumberConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I8AONumberConverter.Tests
{
    /// <summary>
    /// Test methods for conversions to decimals
    /// </summary>
    [TestClass()]
    public class NumberConverterTests
    {
        /// <summary>
        /// Reverses the string test.
        /// </summary>
        [TestMethod()]
        public void ReverseStringTest()
        {
            string test = "hallo";
            string testreverse = NumberConverter.ReverseString(test);
            Assert.AreEqual("ollah", testreverse);
        }

        /// <summary>
        /// Bin to decimal test.
        /// </summary>
        [TestMethod()]
        public void BinToDecTest()
        {
            string test = "1010";
            long dec = NumberConverter.BinToDec(test);
            Assert.AreEqual(10, dec);
        }

        /// <summary>
        /// Bin to decimal test1.
        /// </summary>
        [TestMethod]
        public void BinToDecTest1()
        {
            string test = "11111111111111111111111111111111";
            long dec = NumberConverter.BinToDec(test);
            Assert.AreEqual(4294967295, dec);
        }

        /// <summary>
        /// Bin to decimal test2.
        /// </summary>
        [TestMethod]
        [ExpectedException(exceptionType: typeof(ArgumentException))]
        public void BinToDecTest2()
        {
            string test = "112";
            long dec = NumberConverter.BinToDec(test);
        }

        /// <summary>
        /// Hex to decimal test1.
        /// </summary>
        [TestMethod()]
        public void HexToDecTest1()
        {
            string test = "0x1010";
            long dec = NumberConverter.HexToDec(test);
            Assert.AreEqual(4112, dec);
        }

        /// <summary>
        /// Hex to decimal test2.
        /// </summary>
        [TestMethod()]
        public void HexToDecTest2()
        {
            string test = "0xAF";
            long dec = NumberConverter.HexToDec(test);
            Assert.AreEqual(175, dec);
        }

        /// <summary>
        /// Hex to decimal test3.
        /// </summary>
        [TestMethod()]
        public void HexToDecTest3()
        {
            string test = "0xA1F";
            long dec = NumberConverter.HexToDec(test);
            Assert.AreEqual(2591, dec);
        }

        /// <summary>
        /// Hex to decimal test4.
        /// </summary>
        [TestMethod]
        [ExpectedException(exceptionType:typeof(ArgumentException))]
        public void HexToDecTest4()
        {
            string test = "0x11G";
            long dec = NumberConverter.HexToDec(test);
        }

        /// <summary>
        /// Hex to decimal test5.
        /// </summary>
        [TestMethod]
        [ExpectedException(exceptionType: typeof(ArgumentException))]
        public void HexToDecTest5()
        {
            string test = "111";
            long dec = NumberConverter.HexToDec(test);
        }

        /// <summary>
        /// Hex to decimal test3.
        /// </summary>
        [TestMethod()]
        public void HexToDecTest6()
        {
            string test = "0xFFFFFFFF";
            long dec = NumberConverter.HexToDec(test);
            Assert.AreEqual(4294967295, dec);
        }
    }
}